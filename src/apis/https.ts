import axios from "axios";

let instance = axios.create({
  baseURL: '/api'
});

instance.interceptors.request.use(
    (config) => {
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  instance.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  
  export default async function(method: string, url: string, data = null) {
    let accessToken = 'v3srs6i1veetv3b2dolta9shrmttl72vnfzm220z'
    method = method.toLocaleLowerCase();
  
    switch (method) {
      case "get":
        return instance.get(url, { headers: { authorization: "Bearer " + accessToken }, params: data });
      case "post":
        return instance.post(url, data);
      case "put":
        return instance.put(url, data);
      case "delete":
        return instance.delete(url, { params: data });
    }
  }
  