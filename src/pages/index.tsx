import React from "react";
import LeftContainerComponent from "../components/homePageCompontent/leftContainerComponent"
import RightContainerComponent from "../components/homePageCompontent/rightContainerComponent"

const Home = () => {

  return (
    <div className="flex flex-col md:flex-row container mx-auto w-full h-screen items-center justify-center space-x-4">
      <LeftContainerComponent/>
      <RightContainerComponent/>
    </div>
  );
};

export default Home;
