import 'tailwindcss/tailwind.css'
import '../../styles/global.css'
import { Provider } from "react-redux";
import { useStore } from '../store';
import Layout from '../components/globalComponent/layout/index'
// import mixpanel from 'mixpanel-browser';
// import { MixpanelProvider, MixpanelConsumer } from 'react-mixpanel';
function MyApp({ Component, pageProps }) {

  const store = useStore(pageProps.initialReduxState)

  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  )
}

export default MyApp
