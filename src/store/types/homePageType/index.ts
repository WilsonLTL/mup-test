type userListArray = {
    items: Array<user>
}

type user = {
    _id: string,
    name: {
        last: string,
        first: string
    },
    email: string,
    picture: string,
    location: {
        latitude: Float32Array,
        longitude: any
    }
}

export type homePageType = {
    userList: userListArray,
    selectedUser: user,
    searchingKeyword: string
}
