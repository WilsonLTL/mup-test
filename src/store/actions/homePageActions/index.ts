export const INIT_DATA = "INIT_DATA";
export const UPDATE_SELECTED_USER = "UPDATE_SELECTED_USER";
export const UPDATE_SEARCHING_KEYWORD = "UPDATE_SEARCHING_KEYWORD";

export const initDataHandler = () => ({
   type: INIT_DATA
});

export const updateSelectedUserHandler = () => ({
   type: UPDATE_SELECTED_USER
});

export const updateSearchingKeywordHandler = () => ({
   type: UPDATE_SEARCHING_KEYWORD
})