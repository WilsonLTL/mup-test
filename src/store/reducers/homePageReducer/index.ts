import { PayloadAction } from '@reduxjs/toolkit'
import { INIT_DATA, UPDATE_SELECTED_USER, UPDATE_SEARCHING_KEYWORD } from "../../actions/homePageActions";
import { homePageType } from "../../types/homePageType"

const initState: homePageType = {
  userList: undefined,
  selectedUser: undefined,
  searchingKeyword: ""
}

const homePageReducer = (state = initState, action: PayloadAction<homePageType>) => {
  switch (action.type) {
    case INIT_DATA:
      state.userList = action.payload.userList;
      return { ...state };
    case UPDATE_SELECTED_USER:
      state.selectedUser = action.payload.selectedUser;
      return { ...state };
    case UPDATE_SEARCHING_KEYWORD:
      state.searchingKeyword = action.payload.searchingKeyword;
      return { ...state };
    default:
      return { ...state };
  }
};

export default homePageReducer;
