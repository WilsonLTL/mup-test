import React from "react";
import { useSelector } from "react-redux";
import { LocationOn } from '@material-ui/icons';
import GoogleMapReact from "google-map-react";

const LocationMarkerComponent = (lat, lng) => <LocationOn className="w-8 h-8 text-red-500"/>;

const SimpleMap = () => {
  const home = useSelector((state: any) => state.homePageReducer);
  const zoom = 11;
  return (
    <div className="h-96 w-full">
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyBlO6fro7c66qpRlKYC1zUCqeigViAlORk" }}
        center={
          {
            lat: home.selectedUser.location.latitude, 
            lng: home.selectedUser.location.longitude
          }
        }
        zoom={zoom}
      >
        <LocationMarkerComponent
          lat={home.selectedUser.location.latitude}
          lng={home.selectedUser.location.longitude}
        />
      </GoogleMapReact>
    </div>
  );
};

export default SimpleMap;
