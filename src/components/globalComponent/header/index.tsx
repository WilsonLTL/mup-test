import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { Button, Menu, MenuItem } from "@material-ui/core";
import useTranslation from "next-translate/useTranslation";

const Header = () => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [anchorElLanguage, setAnchorElLanguage] =
    React.useState<null | HTMLElement>(null);
  const router = useRouter();
  const { t } = useTranslation("common");

  const handleClickLanguage = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorElLanguage(event.currentTarget);
  };

  const onClickLanguageandler = (language) => {
    router.push("/" + language);
  };

  const handleCloseLanguage = () => {
    setAnchorElLanguage(null);
  };

  return (
    <div className="absolute inline-flex flex-col h-14 items-end justify-center w-full bg-white shadow">
      <div className="hidden sm:flex">
        <Button onClick={handleClickLanguage} className="mx-2">
          <p className="text-sm font-bold leading-normal text-center text-gray-500">
            {t("Current_Language")}
          </p>
        </Button>
        <Menu
          id="menu"
          className="my-2"
          anchorEl={anchorElLanguage}
          getContentAnchorEl={null}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "right",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
          open={Boolean(anchorElLanguage)}
          onClose={handleCloseLanguage}
        >
          <MenuItem onClick={() => onClickLanguageandler("zh-HK")}>
            <div className="flex items-center space-x-4">
              <span
                className={
                  "text-base leading-normal text-center mx-1 " +
                  (t("Current_Language") == "中文(香港)"
                    ? "text-gray-700"
                    : "text-gray-400")
                }
              >
                中文(香港)
              </span>
            </div>
          </MenuItem>
          <MenuItem onClick={() => onClickLanguageandler("zh-TW")}>
            <div className="flex items-center space-x-4">
              <span
                className={
                  "text-base leading-normal text-center mx-1 " +
                  (t("Current_Language") == "中文(台灣)"
                    ? "text-gray-700"
                    : "text-gray-400")
                }
              >
                中文(台灣)
              </span>
            </div>
          </MenuItem>
          <MenuItem onClick={() => onClickLanguageandler("en")}>
            <div className="flex items-center space-x-4">
              <span
                className={
                  "text-base leading-normal text-center mx-1 " +
                  (t("Current_Language") == "English"
                    ? "text-gray-700"
                    : "text-gray-400")
                }
              >
                English
              </span>
            </div>
          </MenuItem>
        </Menu>
      </div>
    </div>
  );
};

export default Header;
