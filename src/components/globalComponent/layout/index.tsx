import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import Header from "../header";
import { getAllUserData } from "../../../apis/api"

const Layout = ({children}) => {

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: "UPDATE_LOADING_STATUS", paylaod: true })
    getAllUserData().then(res => {
      dispatch({
        type: "INIT_DATA",
        payload: {
          userList:res.data
        }
      })
      dispatch({ type: "UPDATE_LOADING_STATUS", paylaod: false })
    })
  })


  return (
    <div className="w-screen h-screen bg-blueGray-100">
      <Header />
      <div>
        {children}
      </div>
    </div>
  );
}

export default Layout;
