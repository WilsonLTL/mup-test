import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card , CardMedia, Divider, IconButton } from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';
import { ArrowBack } from '@material-ui/icons';
import GoogleMap from "../../../components/globalComponent/googleMap"
import useTranslation from 'next-translate/useTranslation'

const LeftContainerComponent = () => {
  const { t } = useTranslation("common");
  const dispatch = useDispatch();
  const loading = useSelector((state: any) => state.loadingReducer);
  const home = useSelector((state: any) => state.homePageReducer);

  const onClickClearSelectedUserHandler =() => {
    dispatch({ 
      type:"UPDATE_SELECTED_USER",
      payload: {
        selectedUser: undefined
      }
    })
  }

  return (
    <div className={
      "md:flex flex-col space-y-2 w-full md:w-3/5 border rounded-md shadow m-2 p-2 bg-white " + 
      (home.selectedUser == undefined && "hidden")
    }>
      <div className="flex items-center space-x-2">
        <IconButton onClick={onClickClearSelectedUserHandler} size="small">
          <ArrowBack/>
        </IconButton>
        <span className="text-gray-700 text-lg font-extrabold">{t("user_information")}</span>
      </div>
      <Divider />
      {loading.status && home.userList == undefined && (
        <div className="space-y-5">
          <Skeleton variant="rect" height={120} />
          <Skeleton variant="rect" height={300} />
        </div>
      )}
      {!loading.status && home.selectedUser == undefined && (
        <div className="flex flex-col justify-center items-center space-y-2">
          <img
            className="w-96 h-60"
            src="https://freehunter-s3-hk-admin.s3.ap-northeast-2.amazonaws.com/img/chatroom(FL)-new-graphic-nomessage.png.png"
          />
          <span className="text-base text-blueGray-500 font-bold">
            {t("please_select_user")}
          </span>
          <span className="text-sm text-blueGray-500 text-center font-light">
            {t("left_clcik_to_select_user_in_the_user_list_to_find_out_more_user_information")}
          </span>
        </div>
      )}
      {!loading.status && home.selectedUser != undefined && (
        <div className="space-y-4">
          <Card className="flex">
            <CardMedia
              className="w-40"
              image={home.selectedUser.picture}
              title="Live from space album cover"
            />
            <div className="flex flex-col p-4 justify-center space-y-2">
              <div className="text-sm text-gray-700 flex flex-col">
                <span className="text-base font-bold">ID: </span>
                <span>{home.selectedUser._id}</span>
              </div>
              <div className="text-sm text-gray-700 flex flex-col">
                <span className="text-base font-bold">{t("email_address")}: </span>
                <span>{home.selectedUser.email}</span>
              </div>
              <div className="text-sm text-gray-700 flex flex-col">
                <span className="text-base font-bold">{t("user_name")}: </span>
                <span>
                  {home.selectedUser.name.first} {home.selectedUser.name.last}
                </span>
              </div>
              <div className="text-sm text-gray-700 flex flex-col">
                <span className="text-base font-bold">{t("location")}: </span>
                <span>
                  {home.selectedUser.location.latitude}{" "}
                  {home.selectedUser.location.longitude}
                </span>
              </div>
            </div>
          </Card>
          {home.selectedUser.location.latitude != null &&
          home.selectedUser.location.longitude != null ? (
            <Card className="flex">
              <GoogleMap />
            </Card>
          ) : (
            <Card className="flex justify-center p-4">
              <span className="text-lg font-bold text-gray-700">
                {t("the_map_cannot_be_displayed_due_to_missing_location_data")}
              </span>
            </Card>
          )}
        </div>
      )}
    </div>
  );
};

export default LeftContainerComponent;