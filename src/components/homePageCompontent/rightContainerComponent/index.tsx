import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Avatar, Divider, TextField } from "@material-ui/core";
import { Skeleton } from "@material-ui/lab";
import useTranslation from "next-translate/useTranslation";

const RightContainerComponent = () => {
  const { t } = useTranslation("common");
  const dispatch = useDispatch();
  const loading = useSelector((state: any) => state.loadingReducer);
  const home = useSelector((state: any) => state.homePageReducer);

  const onClickUserHandler = (user) => {
    dispatch({ 
      type:"UPDATE_SELECTED_USER",
      payload: {
        selectedUser: user
      }
    })
  }

  const onChangeSearchingKeywordHandler = (event) => {
    dispatch({ 
      type:"UPDATE_SEARCHING_KEYWORD",
      payload: {
        searchingKeyword: event.target.value
      }
    })
  }

  return (
    <div className={
      "flex-col space-y-2 w-full md:w-2/5 h-4/5 md:h-auto border rounded-md shadow m-2 p-2 bg-white " +
      (home.selectedUser == undefined ? "flex" : "hidden md:flex")
    }>
      <div className="flex items-center space-x-4">
        <span className="text-gray-700 text-lg font-extrabold">{t("user_list")}</span>
        <TextField
          onChange={onChangeSearchingKeywordHandler}
          value={home.searchingKeyword}
          margin="dense"
          placeholder={t("enter_ketwords_to_search")}
          variant="outlined"
        />
      </div>
      <Divider />
      <div className="md:h-96 overflow-y-auto space-y-2">
        {loading.status &&
          [...Array(20)].map((x, i) => {
            return (
              <div className="flex items-center space-x-4 border-b p-2 cursor-pointer hover:bg-blueGray-50">
                <Skeleton variant="circle" width={40} height={40} />
                <div className="flex flex-col w-8/12">
                  <Skeleton variant="text" />
                  <Skeleton variant="text" width={80} />
                </div>
              </div>
            );
          })}
        {!loading.status &&
          home.userList != undefined &&
          home.userList.filter(
            user => 
            home.searchingKeyword == "" ||
            user.email.includes(home.searchingKeyword) ||
            user.name.last.includes(home.searchingKeyword) ||
            user.name.first.includes(home.searchingKeyword)
          ).map((x, i) => {
            return (
              <div
                onClick={() => onClickUserHandler(x)}
                className="flex items-center space-x-4 border-b p-2 cursor-pointer hover:bg-blueGray-50"
              >
                <Avatar src={x.picture} />
                <div className="flex flex-col w-8/12">
                  <span className="text-sm text-gray-700">{x.email}</span>
                  <span className="text-xs text-gray-500">
                    {x.name.first} {x.name.last}
                  </span>
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
};

export default RightContainerComponent;
